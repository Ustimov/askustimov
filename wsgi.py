from urllib.parse import parse_qs


def application(env, start_response):
    method = env.get('REQUEST_METHOD', '')
    params = parse_qs(env.get('QUERY_STRING', ''))
    start_response('200 OK', [('Content-Type', 'text/html')])

    response = '<!DOCTYPE html><head><meta charset="utf-8"></head><body>' \
               'Привет, мир!<br>Метод: {0}<br>Параметры: {1}</body></html>'.format(method, params)
    # Python 3
    return [response.encode('utf-8')]
