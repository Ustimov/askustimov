from django.conf.urls import url
from django.views.generic import TemplateView
from .views import hello_world
from . import views

urlpatterns = [
    # url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^$', views.QuestionsById.as_view(), name='index'),
    # url(r'^question/', TemplateView.as_view(template_name='question.html'), name='question'),
    url(r'^question/(?P<id>[0-9]+)/$', views.QuestionDetail.as_view(), name='question'),
    url(r'^tag/(?P<tag>[A-Za-z0-9]+)/$', views.QuestionsByTag.as_view(), name='tag'),
    url(r'^hot/$', views.QuestionsByLikes.as_view(), name='hot'),
    url(r'^ask/$', TemplateView.as_view(template_name='ask.html'), name='ask'),
    url(r'^login/$', TemplateView.as_view(template_name='login.html'), name='login'),
    url(r'^signup/$', TemplateView.as_view(template_name='signup.html'), name='signup'),
    url(r'^hello_world/', hello_world),
]
