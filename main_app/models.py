from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now


class QuestionManager(models.Manager):
    def by_likes_gte(self, likes):
        return self.filter(likes__count__gte=likes).order_by('-likes__count')

    def by_tag(self, tag):
        return self.filter(tags=tag).order_by('-datetime')


class Tag(models.Model):
    text = models.CharField(max_length=32)


class Likes(models.Model):
    count = models.IntegerField(default=0)


class Question(models.Model):
    user = models.ForeignKey(User)
    tags = models.ManyToManyField(Tag)
    likes = models.OneToOneField(Likes)
    title = models.CharField(max_length=128)
    text = models.CharField(max_length=4096)
    datetime = models.DateTimeField(default=now)
    objects = models.Manager()
    manager = QuestionManager()

    def get_absolute_url(self):
        return '/question/{0}/'.format(self.id)


class Answer(models.Model):
    user = models.ForeignKey(User)
    question = models.ForeignKey(Question)
    likes = models.OneToOneField(Likes)
    text = models.CharField(max_length=2048)
    is_correct = models.BooleanField(default=False)
    datetime = models.DateTimeField(default=now)


class Profile(models.Model):
    user = models.OneToOneField(User)
    avatar = models.ImageField()
