from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView
from django.shortcuts import get_object_or_404
from . import models


@csrf_exempt
def hello_world(request):
    method = request.method
    if method == 'GET':
        params = dict(request.GET.lists())
    else:
        params = dict(request.POST.lists())
    response = '<!DOCTYPE html><head><meta charset="utf-8"></head><body>' \
               'Привет, мир!<br>Метод: {0}<br>Параметры: {1}</body></html>'.format(method, params)
    return HttpResponse(response)


class QuestionsById(ListView):
    model = models.Question
    template_name = 'index.html'
    paginate_by = 7
    context_object_name = 'questions'
    ordering = ['-datetime']


class QuestionsByTag(QuestionsById):
    def get_queryset(self):
        self.tag = get_object_or_404(models.Tag, text=self.kwargs['tag'])
        return models.Question.manager.by_tag(self.tag)

    def get_context_data(self, **kwargs):
        context = super(QuestionsByTag, self).get_context_data(**kwargs)
        context['tag'] = self.tag
        return context


class QuestionsByLikes(QuestionsById):
    def get_queryset(self):
        return models.Question.manager.by_likes_gte(0)

    def get_context_data(self, **kwargs):
        context = super(QuestionsByLikes, self).get_context_data(**kwargs)
        context['hot'] = True
        return context


class QuestionDetail(ListView):
    model = models.Answer
    template_name = 'question.html'
    paginate_by = 2
    context_object_name = 'answers'
    ordering = ['-likes__count']

    def get_queryset(self):
        self.question = get_object_or_404(models.Question, id=self.kwargs['id'])
        return models.Answer.objects.filter(question=self.question)

    def get_context_data(self, **kwargs):
        context = super(QuestionDetail, self).get_context_data(**kwargs)
        context['question'] = self.question
        return context
