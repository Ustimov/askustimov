from django.core.management.base import BaseCommand
from main_app import models
from random import randint


class Command(BaseCommand):
    args = '<questions_count>'
    help = 'Creates specified number of questions'

    def handle(self, *args, **options):
        user1, status = models.User.objects.get_or_create(id=1)
        user2, status = models.User.objects.get_or_create(id=2, username='user2')
        tags = []
        for t in range(3):
            tags.append(models.Tag.objects.get_or_create(id=t, text='tag' + str(t))[0])

        questions_count = args[0]
        for i in range(int(questions_count)):
            question = models.Question.objects.create(user=user1,
                                                      likes=models.Likes.objects.create(count=randint(0, 50)),
                                                      title='Question title ' + str(i),
                                                      text='Question text ' + str(i))

            for k in range(randint(1, 3)):
                question.tags.add(tags[k])
                question.save()

            for j in range(randint(1, 5)):
                models.Answer.objects.create(user=user2,
                                             likes=models.Likes.objects.create(count=randint(0, 30)),
                                             question=question,
                                             text='Answer text ' + str(j))

        self.stdout.write('Questions successfully created')
